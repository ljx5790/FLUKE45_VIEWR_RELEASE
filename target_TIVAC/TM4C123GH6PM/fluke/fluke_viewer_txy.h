/*
 * fluke_viewer_txy.h
 *
 *  Created on: 2015年8月9日
 *      Author: acer
 */

#ifndef FLUKE_VIEWER_TXY_H_
#define FLUKE_VIEWER_TXY_H_

#include "../common.h"

/********************初始化设备管脚、晶振等***************************/
extern void initFVT();

/*****************绑定要监视的数据***************************/
extern void bindDataFVT(volatile double *sendData);

#endif /* FLUKE_VIEWER_TXY_H_ */
