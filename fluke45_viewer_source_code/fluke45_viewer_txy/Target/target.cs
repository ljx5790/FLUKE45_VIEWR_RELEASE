﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace fluke45_viewer_txy.Target
{
    class target
    {
        private SerialPort TARGETCOM;   //声明目标设备串口
        private string dataReceived;     //接收数据缓存字符串
        private double val;             //接收转义数值
        private Regex regValue;         //数值正则表达式
        private Match match;            //单个正则表达式匹配结果
        public bool isConnected;       //万用表是否连接上标志
        
        /// 目标设备构造方法，初始化和设备相连的串口
        /// </summary>
        /// <param name="port">端口号</param>
        public target(string port)
        {
            //初始化串口相关参数
            TARGETCOM = new SerialPort(port);
            TARGETCOM.BaudRate = 9600;
            TARGETCOM.Parity = Parity.None;
            TARGETCOM.DataBits = 8;
            TARGETCOM.StopBits = StopBits.One;   //根据参数新建串口
            //指定正则表达式
            regValue = new Regex(@"^[+-]?[0-9]\d*\.\d*|0\.\d*[0-9]\d*$");   //匹配正负浮点数
            val = 0.0;
            dataReceived = null;
        }

        /// <summary>
        /// 目标设备连接测试
        /// </summary>
        /// <returns>true-连接成功</returns>
        private bool TargetConnectTest()
        {
            TARGETCOM.DiscardInBuffer();         //清空缓冲区
            TARGETCOM.WriteLine("CONREQ");      //发送连接测试指令
            System.Threading.Thread.Sleep(100);     //等待延时
            dataReceived = TARGETCOM.ReadExisting();
            TARGETCOM.BaseStream.Flush();
            if (!dataReceived.Contains("target"))
                return false;
            else
                return true;
        }

        /// <summary>
        /// 打开目标设备端口
        /// </summary>
        public void OpenChanelTarget()
        {
            try
            {
                TARGETCOM.Open();
                //isConnected = TargetConnectTest();
                isConnected = true;
            }
            catch
            {
            }
        }

        /// <summary>
        /// 关闭目标设备端口
        /// </summary>
        public void CloseChanelTarget()
        {
            try
            {
                TARGETCOM.Close();
                isConnected = false;
            }
            catch
            {
            }
        }


        public double getTargetValue()
        {
            try
            {
                //TARGETCOM.DiscardInBuffer();
                TARGETCOM.WriteLine("c");            //发送读取指令
                System.Threading.Thread.Sleep(100);
                dataReceived = TARGETCOM.ReadExisting();
                TARGETCOM.BaseStream.Flush();
                if (!dataReceived.Contains("=>"))
                    return 0;
                match = regValue.Match(dataReceived);
                if (match.Success)
                    val = double.Parse(match.Value);
                else
                    val = 0;
                return val;
            }
            catch
            {
                return 0;
            }
        }

    }

}
