﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace fluke45_viewer_txy.Fluke45
{
    class fluke45
    {
        private SerialPort FLUKECOM;    //声明串口功能
        private string dataReceived;     //接收数据缓存字符串
        private double val;             //接收转义数值
        private Regex regValue;         //数值正则表达式
        private Regex regNum;           //编号正则表达式
        private Match match;            //单个正则表达式匹配结果
        public bool isConnected;       //万用表是否连接上标志

        /// <summary>
        /// fluke45构造方法，实现最基本的初始化配置
        /// </summary>
        /// <param name="port">端口号</param>
        public fluke45(string port)
        {
            //初始化串口相关参数
            FLUKECOM = new SerialPort(port);
            FLUKECOM.BaudRate = 9600;
            FLUKECOM.Parity = Parity.None;
            FLUKECOM.DataBits = 8;
            FLUKECOM.StopBits = StopBits.One;   //根据参数新建串口
            //FLUKECOM.ReadBufferSize = 8;
            //指定正则表达式
            regValue = new Regex(@"[+-]?[0-9]*.?[0-9]*E[+-]?[0-9]*");   //（测量值格式：+- x.x E +- x 例如：-3.21E-3）
            regNum = new Regex(@"[0-9]*");  //数值（0~9）
            val = 0.0;
            dataReceived = null;
        }

        /// <summary>
        /// fluke45连接建立测试
        /// 发送ID识别指令，看是否为FLUKE45设备
        /// </summary>
        /// <returns>true-建立成功,false-建立失败</returns>
        private bool FlukeConnectTest()
        {

            //FLUKECOM.DiscardInBuffer();
            //FLUKECOM.DiscardOutBuffer();
            FLUKECOM.WriteLine("*IDN?");    //发送标志识别指令
            System.Threading.Thread.Sleep(500);
            dataReceived = FLUKECOM.ReadExisting();     //读取缓冲区
            FLUKECOM.BaseStream.Flush();
            if (dataReceived.Contains("FLUKE, 45"))
            {
                FLUKECOM.BaseStream.Flush();    //清除缓冲区，识别成功
                return true;
            }
            else
            {
                FLUKECOM.BaseStream.Flush();
                return false;
            }
        }

        /// <summary>
        /// 打开Fluke端口
        /// </summary>
        public void OpenChanelFluke()
        {
            try
            {
                    FLUKECOM.Open();    //打开端口
                    isConnected = FlukeConnectTest();   //测试是否和正确的台表建立连接
                    //isConnected = true;
            }
            catch
            { 
            }
        }

        /// <summary>
        /// Close the channel
        /// </summary>
        public void CloseChannelFluke()
        {
            try
            {
                FLUKECOM.Close();
                isConnected = false;
            }
            catch
            {
            }
        }


        ////////////////////////////////////
        //
        //  以下部分为台表基本操作指令
        //
        ////////////////////////////////////

        public double getFlukeValue()
        {
            try
            {

                FLUKECOM.DiscardInBuffer();
                FLUKECOM.WriteLine("VAL1?");            //发送读取指令
                System.Threading.Thread.Sleep(100);
                dataReceived = FLUKECOM.ReadExisting();
                FLUKECOM.BaseStream.Flush();
                FLUKECOM.DiscardOutBuffer();
                if (!dataReceived.Contains("=>"))
                    return 0;
                match = regValue.Match(dataReceived);   //成功后进行正则转换
                if (match.Success)
                    val = double.Parse(match.Value);
                else
                    val = 0.0;
                return val;
            }
            catch
            {
                return 0;
            }
        }

    }
}
