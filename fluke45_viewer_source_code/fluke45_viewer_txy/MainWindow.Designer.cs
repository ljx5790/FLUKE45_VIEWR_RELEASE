﻿namespace fluke45_viewer_txy
{
    partial class FLUKE
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FLUKE));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.text_data = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ben_sample = new System.Windows.Forms.Button();
            this.ben_analysis = new System.Windows.Forms.Button();
            this.btn_clr = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label_state = new System.Windows.Forms.Label();
            this.dropdown_target = new System.Windows.Forms.ComboBox();
            this.btn_connect = new System.Windows.Forms.Button();
            this.dropdown_fluke = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_about = new System.Windows.Forms.Button();
            this.btn_homepage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::fluke45_viewer_txy.Properties.Resources.LabelFull;
            this.pictureBox1.Location = new System.Drawing.Point(13, 346);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(450, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.text_data);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 327);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "采样数据";
            // 
            // text_data
            // 
            this.text_data.Location = new System.Drawing.Point(7, 21);
            this.text_data.Multiline = true;
            this.text_data.Name = "text_data";
            this.text_data.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.text_data.Size = new System.Drawing.Size(304, 300);
            this.text_data.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ben_sample);
            this.groupBox2.Controls.Add(this.ben_analysis);
            this.groupBox2.Controls.Add(this.btn_clr);
            this.groupBox2.Location = new System.Drawing.Point(337, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(92, 113);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "数据操作";
            // 
            // ben_sample
            // 
            this.ben_sample.Location = new System.Drawing.Point(7, 81);
            this.ben_sample.Name = "ben_sample";
            this.ben_sample.Size = new System.Drawing.Size(75, 23);
            this.ben_sample.TabIndex = 2;
            this.ben_sample.Text = "采样";
            this.ben_sample.UseVisualStyleBackColor = true;
            this.ben_sample.Click += new System.EventHandler(this.ben_sample_Click);
            // 
            // ben_analysis
            // 
            this.ben_analysis.Location = new System.Drawing.Point(7, 51);
            this.ben_analysis.Name = "ben_analysis";
            this.ben_analysis.Size = new System.Drawing.Size(75, 23);
            this.ben_analysis.TabIndex = 1;
            this.ben_analysis.Text = "分析";
            this.ben_analysis.UseVisualStyleBackColor = true;
            this.ben_analysis.Click += new System.EventHandler(this.ben_analysis_Click);
            // 
            // btn_clr
            // 
            this.btn_clr.Location = new System.Drawing.Point(7, 21);
            this.btn_clr.Name = "btn_clr";
            this.btn_clr.Size = new System.Drawing.Size(75, 23);
            this.btn_clr.TabIndex = 0;
            this.btn_clr.Text = "清空";
            this.btn_clr.UseVisualStyleBackColor = true;
            this.btn_clr.Click += new System.EventHandler(this.btn_clr_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label_state);
            this.groupBox3.Controls.Add(this.dropdown_target);
            this.groupBox3.Controls.Add(this.btn_connect);
            this.groupBox3.Controls.Add(this.dropdown_fluke);
            this.groupBox3.Location = new System.Drawing.Point(336, 132);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(93, 123);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "设备连接";
            // 
            // label_state
            // 
            this.label_state.AutoSize = true;
            this.label_state.ForeColor = System.Drawing.Color.Red;
            this.label_state.Location = new System.Drawing.Point(23, 75);
            this.label_state.Name = "label_state";
            this.label_state.Size = new System.Drawing.Size(41, 12);
            this.label_state.TabIndex = 3;
            this.label_state.Text = "未连接";
            // 
            // dropdown_target
            // 
            this.dropdown_target.FormattingEnabled = true;
            this.dropdown_target.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15"});
            this.dropdown_target.Location = new System.Drawing.Point(8, 48);
            this.dropdown_target.Name = "dropdown_target";
            this.dropdown_target.Size = new System.Drawing.Size(75, 20);
            this.dropdown_target.TabIndex = 2;
            this.dropdown_target.Text = "目标设备";
            this.dropdown_target.SelectedIndexChanged += new System.EventHandler(this.dropdown_target_SelectedIndexChanged);
            // 
            // btn_connect
            // 
            this.btn_connect.Location = new System.Drawing.Point(6, 94);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(75, 23);
            this.btn_connect.TabIndex = 1;
            this.btn_connect.Text = "连接";
            this.btn_connect.UseVisualStyleBackColor = true;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // dropdown_fluke
            // 
            this.dropdown_fluke.FormattingEnabled = true;
            this.dropdown_fluke.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15"});
            this.dropdown_fluke.Location = new System.Drawing.Point(7, 21);
            this.dropdown_fluke.Name = "dropdown_fluke";
            this.dropdown_fluke.Size = new System.Drawing.Size(75, 20);
            this.dropdown_fluke.TabIndex = 0;
            this.dropdown_fluke.Text = "FLUKE45";
            this.dropdown_fluke.SelectedIndexChanged += new System.EventHandler(this.dropdown_fluke_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_about);
            this.groupBox4.Controls.Add(this.btn_homepage);
            this.groupBox4.Location = new System.Drawing.Point(337, 261);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(92, 79);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "扩展功能";
            // 
            // btn_about
            // 
            this.btn_about.Location = new System.Drawing.Point(5, 49);
            this.btn_about.Name = "btn_about";
            this.btn_about.Size = new System.Drawing.Size(75, 23);
            this.btn_about.TabIndex = 2;
            this.btn_about.Text = "关于程序";
            this.btn_about.UseVisualStyleBackColor = true;
            this.btn_about.Click += new System.EventHandler(this.button7_Click);
            // 
            // btn_homepage
            // 
            this.btn_homepage.Location = new System.Drawing.Point(5, 20);
            this.btn_homepage.Name = "btn_homepage";
            this.btn_homepage.Size = new System.Drawing.Size(75, 23);
            this.btn_homepage.TabIndex = 1;
            this.btn_homepage.Text = "访问主页";
            this.btn_homepage.UseVisualStyleBackColor = true;
            this.btn_homepage.Click += new System.EventHandler(this.button6_Click);
            // 
            // FLUKE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 390);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FLUKE";
            this.Text = "FLUKE45 VIEWER";
            this.Load += new System.EventHandler(this.FLUKE_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox text_data;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button ben_sample;
        private System.Windows.Forms.Button ben_analysis;
        private System.Windows.Forms.Button btn_clr;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox dropdown_fluke;
        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn_about;
        private System.Windows.Forms.Button btn_homepage;
        private System.Windows.Forms.ComboBox dropdown_target;
        private System.Windows.Forms.Label label_state;
    }
}

