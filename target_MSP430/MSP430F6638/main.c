 /* 
 *
 * fluke45 viewer 使用示例代码
 *
 *  Created on: 2015年5月11日
 *      Author: tanxiaoyao
 */
#include <msp430f6638.h>
#include "fluke_viewer_txy.h"
/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    volatile double testValue = 0.1;	
	//请尽量将变量设置为volatile类型的全局变量，
	//这样单片机会强制读取实时内存值而不是优化到寄存器中运行，
	//这样在上位机读取中断中读到的才是变量的实时值

    initFVT();					//初始化驱动
    bindDataFVT(&testValue);	//绑定要发送的变量

    while(1)					//主循环模拟一个不断变化的全局变量
    {
    	if(testValue < 9999)
    		testValue += testValue;
    	else
    		testValue = 0.1;
    }

	return 0;
}
